import math

def discrimiante (a, b, c):
    dis = pow(b,2)-(4*a*c)
    return dis

def raices (a, b, dis):
    raiz1 = (-b+math.sqrt(dis))/(2*a)
    raiz2 = (-b-math.sqrt(dis))/(2*a)
    return raiz1, raiz2

print("Cálculadora de Raíces")
a = float(input("Valor de a: "))
b = float(input("Valor de b: "))
c = float(input("Valor de c: "))

disc = discrimiante(a, b, c)

if disc < 0:
    print("No hay raíces")
else:
    print("Las raíces son: ", raices(a, b, disc))